import java.io.*;

public class TestR {

    public static void main(String []args) {
       String directory = System.getProperty("user.dir");
        String fileName = "readme.txt";
        String absolutePath = directory + File.separator + fileName;

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(absolutePath))) {
            String line = bufferedReader.readLine();
            while (line != null) {
                System.out.println(line);
                line = bufferedReader.readLine();
            }
        } catch (FileNotFoundException e) {
            System.out.println(e);
        } catch (IOException e) {
            System.out.println(e);
        }

        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(absolutePath))) {
            String fileContent = "Test123";
            bufferedWriter.write(fileContent);
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}
